const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs')
const passport =require('passport')

let User = require('../models/user')


router.get('/register',(req,res)=>{
  res.render('user_edit', {
    title: "Register User",
    action: "/users/create"
  })
})

router.get('/edit/:id',(req,res)=>{
  User.findById(req.params.id,(err,data)=>{
    res.render('user_edit',{
      title: "Edit User",
      action: "/users/update/" + data._id,
      user: data
    })
  })
})

router.post('/create',(req,res)=>{
  req.checkBody('name','Name field cannot be empty').notEmpty().trim().escape()
  req.checkBody('email','Email field cannot be empty').notEmpty().isEmail().trim().escape()
  req.checkBody('username','Username field cannot be empty').notEmpty().trim().escape()
  req.checkBody('password','Password field cannot be empty').notEmpty()
  req.checkBody('password','Passwords must match').equals(req.body.password)
  let errors = req.validationErrors();
  if (errors){
    res.render('user_edit',{
      title: "Resgister User",
      action: "/users/create",
      data: {
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
      },
      errors: errors
    })
  }
  let user = new User()
  user.name=req.body.name
  user.email=req.body.email
  user.username = req.body.username
  user.password = req.body.password
  bcrypt.genSalt(10, function(err, salt){
    bcrypt.hash(user.password, salt, function(err, hash){
      if (err){
        console.log(err)
      } else {
        user.password = hash
        user.save(function(err){
           if (err){
             console.log(err)
             return
           }
           req.flash('success','User created successfully.')
           res.redirect('/users/login')
        })
      }
    })
  })
})

router.get('/login', (req,res)=>{
  res.render('login',{
    title: "Login"
  })
})
router.post('/login', (req,res, next)=>{
  console.log('login info posted...')
  passport.authenticate('local',{
    successRedirect: '/',
    failureRedirect: '/users/login',
    failureFlash: true
  })(req, res, next)
})


module.exports= router
