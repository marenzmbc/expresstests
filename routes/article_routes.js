const express = require('express')
const router = express.Router()

let Article = require('../models/article')
let User = require('../models/user')


router.get('/new',ensureAuthenticated, (req,res)=>{
  res.render('article_edit', {
    title: "New Article",
    action: "/articles/create"
  })
})

router.get('/edit/:id',ensureAuthenticated, (req,res)=>{
  Article.findById(req.params.id,(err,data)=>{
    res.render('article_edit',{
      title: "Edit Article",
      action: "/articles/update/" + data._id,
      article: data
    })
  })
})

router.post('/create',ensureAuthenticated, (req,res)=>{
  console.log('***> ' + req.user._id)
  req.checkBody('title','Title field cannot be empty').notEmpty().trim().escape()
  req.checkBody('body','Body field cannot be empty').notEmpty().trim().escape()
  let errors = req.validationErrors();
  if (errors){
    res.render('article_edit',{
      title: "New Article",
      action: "/articles/create",
      data: {
        title: req.body.title,
        body: req.body.body
      },
      errors: errors
    })
  }
  let article = new Article()
  article.title=req.body.title
  article.author=req.user._id
  article.body = req.body.body
  article.save(function(err){
     if (err){
       console.log(err)
       return
     }
     req.flash('success','Article created successfully.')
     res.redirect('/')
  })
})

router.post('/update/:id', ensureAuthenticated, (req,res)=>{
  let article = {}
  article.title=req.body.title
  article.author=req.body.author
  article.body = req.body.body
  let query = {_id: req.params.id}
  Article.update(query, article,(err)=>{

    if (err){
      console.log(err)
      return
    }
    req.flash('success','Article updated successfully.')
    res.redirect('/')
  })
})

router.delete('/delete/:id', ensureAuthenticated, (req,res)=>{
  if (!req.user._id){
    res.status(500).send()
  }
  let query = {_id: req.params.id}
  Article.findById(req.params.id, (err, article)=>{
    if (article.author!=req.user._id){
      res.status(500).send()
    } else {
      Article.deleteOne(query, function(err){
        if (err){
          console.log(err)
          return
        }
        res.send('Success')
      })
    }
  })
})

/* SHOW */
router.get('/:id',(req,res)=>{
  Article.findById(req.params.id,(err,data)=>{
    User.findById(data.author,(err,userdata)=>{
      res.render('article_show',{
        title: data.title,
        author: userdata.username,
        article: data,
        errors: err
      })
    })
  })
})

// Access Control
function ensureAuthenticated(req, res, next){
  if(req.isAuthenticated()){
    return next()
  } else {
    req.flash('danger', 'You must be logged in to do this.')
    res.redirect('users/login')
  }
}

module.exports= router
