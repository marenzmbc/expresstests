const express = require('express')
const path = require('path')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const expressValidator = require('express-validator')
const flash = require('connect-flash')
const session = require('express-session')
const passport = require('passport')
const config = require('./config/database')

//mongoose.connect(config.database,  { useNewUrlParser: true })
mongoose.connect(config.database);
let db = mongoose.connection

/* DB Check connectivity */
db.once('open',()=>{
  console.log('DB Connected...')
})
/* DB Error Handling */
db.on('error', function(err){
  console.log(err)
})

const app=express()

let Article = require('./models/article')

/* LOAD VIEW ENGINE */
app.set('views',path.join(__dirname, 'views') )
app.set('view engine', 'pug')
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

/* STATIC : PUB */
app.use(express.static(path.join(__dirname, 'public')))

/* EXPRESS SESSION */
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}))
app.use(require('connect-flash')())
app.use(function(req,res,next){
  res.locals.messages= require('express-messages')(req,res)
  next()
})

/* VALIDATOR */
app.use(expressValidator());

//Passport Config Middleware
require('./config/passport')(passport)
app.use(passport.initialize())
app.use(passport.session())
app.get('*', (req, res, next)=>{
  res.locals.user = req.user || null
  next()
})

/* ROUTES */
app.get('/',(req, res)=>{
  let myVar = 'This is a string!'
  Article.find({},(err, data)=>{
    if (err){
      console.log(err)
    } else {
      res.render('index',{
        title: "ExpCRUD2",
        articles: data
      })
    }
  })
})

/* ADD ARTICLES ROUTES */
let article_routes = require('./routes/article_routes')
app.use('/articles', article_routes)

let user_routes = require('./routes/user_routes')
app.use('/users', user_routes)

app.get('/logout', (req,res)=>{
  req.logout()
  req.flash('success','You have successfully logged out.')
  res.redirect('/users/login')
})

/* START SERVER */
app.listen(3000,function(){
  console.log('Server is listening')
})
