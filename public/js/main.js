$(function(){
  $('.btn-delete').click(function(e){
    e.preventDefault()
    const dataID=$(e.target).attr('data-id')
    $.ajax({
      type: 'DELETE',
      url: '/articles/delete/' + dataID,
      success: function(response){
        alert('deleting article')
        window.location.href='/'
      },
      error: function(err){
        console.log(err)
      }
    })
  })
})
