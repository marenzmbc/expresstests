https://www.youtube.com/watch?v=m-gLLTbBEE4&list=PLillGF-RfqbYRpji8t4SxUkMxfowG4Kqp&index=11
https://github.com/bradtraversy/nodekb


http://expresstests.markmakesstuff.com


Start Mongo DB server
  ~/mongodb/bin/mongod

Use Mongo DB client
  ~/mongodb/bin/mongo
  Stop:do service mongod stop
  Restart: tsudo service mongod restart

Converting Bootstrap code to HTML, use this:
  https://html-to-pug.com/

  Clone project
    git clone git@bitbucket.org:marenzmbc/expresstests.gitps://html-to-pug.com/


DEPLOYMENT
  Set up DigitalOcean droplet (ubuntu-s-1vcpu-1gb-nyc1-01)
  Add SSH public key
    Get public key from machine:
      cat ~/.ssh/id_rsa.pub | pbcopy
    If it doesn't exist, run: ssh-keygen -t rsa -b 4096
  SSH in
    ssh root@165.22.5.131
  Install node.js
    curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
    sudo apt install nodejs

  Install MongoDB
    https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
    echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
    sudo apt-get update
    sudo apt-get install -y mongodb-org

  Start MongoDB as Service: sudo service mongod start
  Stop:sudo service mongod stop
  Restart: sudo service mongod restart

  Set up git remote
    mkdir expresstests
    cd expresstests
    mkdir project.git
    cd project.git
    git init --bare
    nano hooks/post-receive
      #!/bin/bash
      GIT_WORK_TREE=/home/201008/users/.home/domains/greenfieldin.matchbook-creative.com/html git checkout -f
  - chmod +x hooks/post-receive
  - On local machine, add a git remote with this URL: ssh://root@165.22.5.131/home/201008/users/.home/domains/greenfieldin.matchbook-creative.com/html/project.git
  - Check via ssh or FTP to ensure the files are there.
